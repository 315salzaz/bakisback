//Projektas: Linker4you Back-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Notes Schema for saving user and admin notes
//Code Line: 22
const mongoose = require("mongoose");

const NotesShema = mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type:String,
        required: true,
    },
    _userId: {
        type: mongoose.Types.ObjectId,
        required: true
    }
});

module.exports = mongoose.model('Notes', NotesShema);

