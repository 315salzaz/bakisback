//Projektas: Linker4you Back-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Schema for Link storing
//Code Line:30
const mongoose = require("mongoose");
const shortId = require('shortid')

const LinkShema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    fullUrl: {
        type: String,
        required: true
    },
    shortUrl: {
        type: String,
        required: true,
        unique: true,
        default: shortId.generate
    },
    clicks: {
        type: [mongoose.Types.ObjectId]
    },
    favoriteGraphs: {
        type: [mongoose.Types.ObjectId]
    },
    _userId: {
        type: mongoose.Types.ObjectId,
        required: true
    }
});

module.exports = mongoose.model('Link', LinkShema);