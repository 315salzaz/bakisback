//Projektas: Linker4you Back-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Schema saving device information from the visting person
//Code Line:35
const mongoose = require("mongoose");

const DevicesShema = mongoose.Schema({
    tabletCount: {
        type: Number,
        default: 0
    },
    mobileCount: {
        type: Number,
        default: 0
    },
    desktopCount: {
        type: Number,
        default: 0
    }
})

DevicesShema.methods.addDevice = function (deviceType) {
    const device = this
    switch (deviceType) {
        case "tablet":
            device.tabletCount++;
            return;
        case "mobile":
            device.mobileCount++;
            return;
        case "desktop":
            device.desktopCount++;
            return;
        default:
            return;
    }
}

module.exports = mongoose.model('Devices', DevicesShema);