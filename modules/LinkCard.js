//Projektas: Linker4you Back-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Schema for Link_Card storing
//Code Line:20
const mongoose = require("mongoose");

const linkCardShema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    datasetLabel: {
        type: String,
        default: ""
    },
    infoType: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('LinkCard', linkCardShema);