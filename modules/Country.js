//Projektas: Linker4you Back-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Schema saving country information from the visting person
//Code Line:20
const mongoose = require("mongoose");

const CountryShema = mongoose.Schema({
    countryCode: {
        type: String,
        required: true
    },
    clickCount: {
        type: Number,
        default: 1
    }
})

CountryShema.statics.getCountryInfo = function() {
    return "Not Implemented"
}

module.exports = mongoose.model('Country', CountryShema);