//Projektas: Linker4you Back-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Schema for receiving clicks
//Code Line:18
const mongoose = require("mongoose");

const ClicksIntervalShema = mongoose.Schema({
    timeInterval: {
        type: Date,
        required: true
    },
    clickCount: {
        type: Number,
        default: 1
    },
    countriesInfo: {
        type: [mongoose.Types.ObjectId]
    },
    devicesInfo: {
        type: mongoose.Types.ObjectId
    },
});

module.exports = mongoose.model('ClicksInterval', ClicksIntervalShema);