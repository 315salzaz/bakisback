//Projektas: Linker4you Back-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: All of the routings and autentification implementing for users to get only specific information
//Code Line: 588
const express = require('express')
const {mongoose} = require('./db/mongoose');
const ShortUrl = require('./modules/shortUrl')
const LinkCard = require('./modules/LinkCard')
const Link = require('./modules/Link')
const ClicksInterval = require('./modules/ClicksInterval')
const Devices = require('./modules/Devices')
const Country = require('./modules/Country')
const User = require('./modules/User')
const app = express()
const jwt = require('jsonwebtoken');
const Notes = require('./modules/Notes')
const bodyparser = require('body-parser');
const DailyChecker = require('./services/dailyChecker.service');
app.listen(3000);



/* Robot created to check if the link got pressed double time than previous day to send and email for user */
const DATE_DAY = 1000*60*60*24

const dailyChecker = new DailyChecker(doDailyCheck)

async function doDailyCheck () {
  const allUsers = await User.find()
  const currentDate = new Date()

  for(var user of allUsers) {
    const links = await Link.find({_userId: user._id})

    for(var link of links) {
      var todayCounter = 0
      var yesterdayCounter = 0
      
      for(var intervalId of link.clicks) {
        const interval = await ClicksInterval.findById(intervalId)

        if (interval.timeInterval.getTime() / DATE_DAY + 1 >= currentDate.getTime() / DATE_DAY) 
          yesterdayCounter += interval.clickCount

        if (interval.timeInterval.getTime() / DATE_DAY + 2 >= currentDate.getTime() / DATE_DAY - 1)
          todayCounter += interval.clickCount
      }

      if(todayCounter > yesterdayCounter * 2 && yesterdayCounter != 0)
        DailyChecker.sendEmail(user.email, link.title, link.fullUrl, yesterdayCounter, todayCounter)
    }
  }
}

/* Middleware*/
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token, x-refresh-token, _id, email");
  res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
  res.header(
    'Access-Control-Expose-Headers',
    'x-access-token, x-refresh-token'
  );
  
  next();
});

let authenticate = (req, res, next) => {
  let token = req.header('x-access-token');

  // verify the JWT
  jwt.verify(token, User.getJWTSecret(), (err, decoded) => {
      if (err) {
          // jwt is invalid 
          res.status(401).send(err);
      } else {
          // jwt is valid
          req.user_id = decoded._id;
          next();
      }
  });
}

/* load middleware*/
app.use(bodyparser.json());

// Verify Refresh Token Middleware
let verifySession = (req, res, next) => {
  // grab the refresh token from the header
  let refreshToken = req.header('x-refresh-token');

  // grab the _id from the header
  let _id = req.header('_id');

  User.findByIdAndToken(_id, refreshToken).then((user) => {
      if (!user) {
          // user not found
          return Promise.reject({
              'error': 'User not found. Make sure that the refresh token and user id are correct'
          });
      }


      // if the code reaches here - the user was found
      // therefore the refresh token exists in the database - but we still have to check if it has expired or not

      req.user_id = user._id;
      req.userObject = user;
      req.refreshToken = refreshToken;

      let isSessionValid = false;

      user.sessions.forEach((session) => {
          if (session.token === refreshToken) {
              // check if the session has expired
              if (User.hasRefreshTokenExpired(session.expiresAt) === false) {
                  // refresh token has not expired
                  isSessionValid = true;
              }
          }
      });

      if (isSessionValid) {
          // the session is VALID
          next();
      } else {
          // the session is not valid
          return Promise.reject({
              'error': 'Refresh token has expired or the session is invalid'
          })
      }

  }).catch((e) => {
      res.status(401).send(e);
  })
}

/* route modules */
// Creating LinkCardInformation
app.post('/LinkCard_Post_LinkCardInfo', async (req, res) => {
  const linkCard = await LinkCard.create({ 
    title: req.body.title,
    type: req.body.type,
    datasetLabel: req.body.datasetLabel,
    infoType: req.body.infoType
  })


  const link = await Link.findOne({ shortUrl: req.body.shortUrl})
  console.log(link)
  link.favoriteGraphs.push(linkCard._id)
  link.save()

  res.status(200).json(linkCard)
})
app.post('/home/navigation-bar/links/account/notes', authenticate, async (req, res) => {
    await Notes.create({ 
    title: req.body.title,
    description: req.body.description,
    _userId: req.user_id
  }).then((resp) => res.status(200).json(resp))
})
app.get('/home/navigation-bar/links/account/notes',authenticate, async(req, res) => {
  await Notes.find({_userId: req.user_id})
  .then((resp) => res.status(200).json(resp))
})
app.delete('/home/navigation-bar/links/account/notes/:id', authenticate, (req, res) => {
  // We want to delete the specified list (document with shortUrl in the URL)
  Notes.findOneAndRemove({
       _id: req.params.id,
  }).then((removedNoteDoc) => {
      res.send(removedNoteDoc);
  })
});
app.patch('/home/navigation-bar/links/account/notes/:id', authenticate, (req, res) => {
  // We want to update the specified list (list document with id in the URL) with the new values specified in the JSON body of the request
  Notes.findOneAndUpdate({ _id: req.params.id, _userId: req.user_id }, {
      $set: req.body
  }).then(() => {
      res.send({ 'message': 'updated successfully'});
  });
});
// Getting LinCard ifnormation by shortUrl
app.get('/LinkCard_Get_LinkCardInfos/:shortUrl', async(req, res) => {
  const link = await Link.findOne({ shortUrl: req.params.shortUrl})

  var graphs = []
  for(var currentId of link.favoriteGraphs)
    graphs.push(await LinkCard.findById(currentId))

  res.status(200).send(graphs)
})
//Deleting Lincard by  graph Title
app.delete('/LinkCard_Delete_DeleteGraphByTitle/:shortUrl/:graphTitle', async(req, res) => {
  const link = await Link.findOne({shortUrl: req.params.shortUrl})

  for(var idIndex in link.favoriteGraphs) {
    const card = await LinkCard.findById(link.favoriteGraphs[idIndex])
    if (req.params.graphTitle === card.title) {
      await LinkCard.findByIdAndDelete(link.favoriteGraphs[idIndex])
      link.favoriteGraphs.splice(idIndex, 1)
      link.save()

      res.status(200).send(card)
      return
    }
  }
})
// Geting all of the linkCard information of the user
app.get('/LinkCard_Get_All', async(req, res) => {
  await LinkCard.find()
  .then((resp) => res.status(200).json(resp))
})
// Deliting all of the user linkCards
app.delete('/LinkCard_Delete_WipeAll', async(req, res) => {
  const cards = await LinkCard.find()
  for(var _ in cards)
    await LinkCard.findOneAndDelete()
  res.status(200).send("Deleted")
})
//Deleting all if the linkcard information according to the shortURl
app.delete('/LinkCard_Delete_WipeAllInLink/:shortUrl', async(req, res) => {
  const link = await Link.findOne({shortUrl: req.params.shortUrl})

  for(var index of link.favoriteGraphs) {
    await LinkCard.findByIdAndDelete(index)
  }

  link.favoriteGraphs.splice(0, link.favoriteGraphs.length)
  link.save()
  res.status(200).send("Deleted")
})
//Deleting all of the Link information
app.delete('/Link_Delete_WipeAll', authenticate, async(req, res) => {
  const links = await Link.find({
    _userId:req.user_id
  })
  for(var _ in links)
    await Link.findOneAndDelete()
  res.status(200).send("Deleted")
})
app.delete('/Link_Delete_WipeAll/:_shortUrl', authenticate, (req, res) => {
  // We want to delete the specified list (document with shortUrl in the URL)
  Link.findOneAndRemove({
       shortUrl: req.params._shortUrl,
      _userId: req.user_id
  }).then((removedListDoc) => {
      res.send(removedListDoc);
  })
});
let deletelinkcardfromlist = (_shortUrl) => {
  LinkCard.deleteMany({
     _shortUrl
  }).then(() => {
      console.log("LinkCards from " + _shortUrl + " were deleted!");
  })
}

// Creating new link 
app.post('/Link_Post_NewLink', authenticate, async (req, res) => {
  await Link.create({ 
    title: req.body.title,
    fullUrl: req.body.fullUrl, 
    _userId: req.user_id,
    shortUrl: req.body.shortUrl
  }).then((resp) => res.status(200).json(resp))
})
// Getting all of the links for the auth user
app.get('/Link_Get_All', authenticate, async (req, res) => {
  await Link.find({
    _userId: req.user_id
  })
  .then((resp) => res.status(200).json(resp))
})
app.get('/Link_Get_All/:id', authenticate, async (req, res) => {
  await Link.findOne({
    _userId: req.user_id,
    _id:req.params.id
  })
  .then((resp) => res.status(200).json(resp))
})
app.patch('/Link_Get_All/:shortUrl', authenticate, (req, res) => {
  // We want to update the specified list (list document with id in the URL) with the new values specified in the JSON body of the request
  Link.findOneAndUpdate({ shortUrl: req.params.shortUrl, _userId: req.user_id }, {
      $set: req.body
  }).then(() => {
      res.send({ 'message': 'updated successfully'});
  });
});
// Getting all of the links for the auth user home screen
app.get('/home/navigation-bar/links', authenticate, async (req, res) => {
  await Link.find({
    _userId: req.user_id
  })
  .then((resp) => res.status(200).json(resp))
})
  
app.post('/home/navigation-bar/links', async (req, res) => {
  await ShortUrl.create({ full: req.body.full })
  .then((resp) => res.status(200).json(resp))
})
// Geting click information by shorurl 
app.get('/ClickInterval_Get_WeekOfLink/:shortUrl', async(req, res) => {
  const link = await Link.findOne({ shortUrl: req.params.shortUrl })

  const currentDate = new Date()
  var weekIntervals = []
  for(var intervalId of link.clicks) {
    const interval = await ClicksInterval.findById(intervalId)

    if ((interval.timeInterval.getTime() / DATE_DAY + 7) >= currentDate.getTime() / DATE_DAY) {
      var devicesInfo = await Devices.findById(interval.devicesInfo)

      var countriesInfo = []
      for(var countryId of interval.countriesInfo) {
        countriesInfo.push(await Country.findById(countryId))
      }
      
      weekIntervals.push({
        timeInterval: interval.timeInterval, 
        clickCount: interval.clickCount, 
        devicesInfo: devicesInfo,
        countriesInfo: countriesInfo
      })
    }
  }

  res.status(200).json(weekIntervals)
})
// Redirecting user by short URl
app.get('/r/:shortUrl/:device/:countryData', async (req, res) => {
  const shortUrl = await Link.findOne({ shortUrl: req.params.shortUrl })
  if (shortUrl == null) return res.sendStatus(404)

  res.redirect(shortUrl.fullUrl)

  let currentDate = new Date();
  currentDate.setMinutes(0, 0, 0);
  var lastInterval = null;
  for(var c of shortUrl.clicks) {
    let cInterval = await ClicksInterval.findById(c);
    
    if (cInterval != null && cInterval.timeInterval.getTime() === currentDate.getTime()) {
      lastInterval = cInterval;
      break;
    }
  }

  if (lastInterval != null) {
    lastInterval.clickCount++;
    lastInterval.save();
  } else {
    const newClicks = await ClicksInterval.create({ timeInterval: currentDate });
    shortUrl.clicks.push(newClicks._id);
    lastInterval = newClicks;
  }

  var currentCountry = null;
  for(var countryId of lastInterval.countriesInfo) {
    currentCountry = await Country.findById(countryId)
    if(currentCountry != null)
      break;
  }
  if (currentCountry != null) {
    currentCountry.clickCount++;
    currentCountry.save();
  } else {
    const newCountry = await Country.create({ countryCode: req.params.countryData })
    lastInterval.countriesInfo.push(newCountry)
  }

  var currentDevices = await Devices.findById(lastInterval.devicesInfo)
  if (currentDevices == null) {
    const newDevices = await Devices.create({});
    lastInterval.devicesInfo = newDevices;
    currentDevices = newDevices;
  }
  currentDevices.addDevice(req.params.device);
  currentDevices.save();
  lastInterval.save()
  shortUrl.save()
})
  
app.delete('/home/navigation-bar/links/:shortUrl', ( req,res) => {
  ShortUrl.findOneAndRemove({
    short: req.params.shortUrl
  }).then((removelink) => {
    res.send(removelink);
  })
})
/* User Routes */
app.post('/users', (req, res) => {
  // User sign up

  let body = req.body;
  let newUser = new User(body);

  newUser.save().then(() => {
      return newUser.createSession();
  }).then((refreshToken) => {
      // Session created successfully - refreshToken returned.
      // no geneate an access auth token for the user

      return newUser.generateAccessAuthToken().then((accessToken) => {
          // access auth token generated successfully, now return an object containing the auth tokens
          return { accessToken, refreshToken }
      });
  }).then((authTokens) => {
      // Now construct and send the response to the user with their auth tokens in the header and the user object in the body
      res
          .header('x-refresh-token', authTokens.refreshToken)
          .header('x-access-token', authTokens.accessToken)
          .send(newUser);
  }).catch((e) => {
      res.status(400).send(e);
  })
})
app.get('/home/navigation-bar/links/account/:id', authenticate, async (req, res) => {
  console.log(req.params.id);
  await User.findById(req.params.id)
  .then(userFound => {
    if(!userFound) {return res.status(404).end();}
    return res.status(200).json(userFound);
  })
  .catch(err => next(err));
})
app.get('/home/navigation-bar/links/message/:id', authenticate, async (req, res) => {
  console.log(req.params.id);
  await User.findById(req.params.id)
  .then(userFound => {
    if(!userFound) {return res.status(404).end();}
    return res.status(200).json(userFound.email);
  })
  .catch(err => next(err));
})
/* Admin Panel*/
// Finding all of the information of the users by user who have admin set up
app.get('/admin/accounts/:id', authenticate, async (req,res) =>{
  await User.findById(req.params.id)
  .then(userFound => {
    console.log(userFound)
    if(!userFound) {return res.status(404).end();}
    if(userFound.isAdmin)
    {
      User.find().then(resp => {
        return res.status(200).json(resp);
      })     
    }
     console.log("you are not admin")
  })
})
// Deleting user from user list by admin
app.delete('/admin/accounts/delete/:id/:_id', authenticate, async (req,res) =>{
  await User.findById(req.params.id)
  .then(userFound => {
    console.log(userFound)
    if(!userFound) {return res.status(404).end();}
    if(userFound.isAdmin)
    {
      User.findByIdAndRemove({
        _id: req.params._id,
   }).then((removeduser) => {
       res.send(removeduser);
   })
      }
     console.log("you are not admin")
  })
})
// Updating user email  by admin
app.patch('/admin/accounts/update/:id/:_id', authenticate, async (req,res) =>{
  await User.findById(req.params.id)
  .then(userFound => {
    console.log(userFound)
    if(!userFound) {return res.status(404).end();}
    if(userFound.isAdmin)
    User.findByIdAndUpdate({ 
      _id: req.params._id}, {
      $set: req.body
  }).then(() => {
      res.send({ 'account': 'updated successfully'});
  });
     console.log("you are not admin")
  })
})
// Getting all of the link information from all users by admin
app.get('/admin/accounts/links/:id', authenticate, async (req,res) =>{
  await User.findById(req.params.id)
  .then(userFound => {
    console.log(userFound)
    if(!userFound) {return res.status(404).end();}
    if(userFound.isAdmin)
    Link.find({ 
      
  }).then((resp) => {
    return res.status(200).json(resp);
  });
     console.log("you are not admin")
  })
})
// Deleting one particular link from the user link list
app.delete('/admin/accounts/links/delete/:id/:_id', authenticate, async (req,res) =>{
  await User.findById(req.params.id)
  .then(userFound => {
    console.log(userFound)
    if(!userFound) {return res.status(404).end();}
    if(userFound.isAdmin)
    {
      Link.findByIdAndRemove({
        _id: req.params._id,
   }).then((removeduser) => {
       res.send(removeduser);
   })
      }
     console.log("you are not admin")
  })
})
// Updating link information by admin
app.patch('/admin/accounts/links/update/:id/:_id', authenticate, async (req,res) =>{
  await User.findById(req.params.id)
  .then(userFound => {
    console.log(userFound)
    if(!userFound) {return res.status(404).end();}
    if(userFound.isAdmin)
    Link.findByIdAndUpdate({ 
      _id: req.params._id}, {
      $set: req.body
  }).then(() => {
      res.send({ 'link': 'updated successfully'});
  });
     console.log("you are not admin")
  })
})
/* Login */
app.post('/users/login', (req, res) => {
  let email = req.body.email;
  let password = req.body.password;

  User.findByCredentials(email, password).then((user) => {
      return user.createSession().then((refreshToken) => {
          // Session created successfully - refreshToken returned.
          // now geneate an access auth token for the user

          return user.generateAccessAuthToken().then((accessToken) => {
              // access auth token generated successfully, now return an object containing the auth tokens
              return { accessToken, refreshToken }
          });
      }).then((authTokens) => {
          // Now construct and send the response to the user with their auth tokens in the header and the user object in the body
          res
              .header('x-refresh-token', authTokens.refreshToken)
              .header('x-access-token', authTokens.accessToken)
              .send(user);
      })
  }).catch((e) => {
      res.status(400).send(e);
  });
})
// Generates and returns an access token

app.get('/users/me/access-token', verifySession, (req, res) => {
   req.userObject.generateAccessAuthToken().then((accessToken) => {
       res.header('x-access-token', accessToken).send({ accessToken });
   }).catch((e) => {
       res.status(400).send(e);
   });
})
app.get('/',(req,res) => {
    res.send("hello world");
})
const server = require('http').createServer();
const options = {  cors:true,
  origins:["http://127.0.0.1:5347"],};
const io =require('socket.io')(server, options);
server.listen(3001)
/* Socket configuration */
io.on('connection', (socket) => {
  
  socket.on('disconnect', function(){
    io.emit('users-changed', {user: socket.username, event:'left'})
  })
  socket.on('set-name', (name)=>{
    socket.username = name;
    io.emit('users-changed', {user: name, event: 'joined'})
  })
  socket.on('send-message', (message) => {
    io.emit('message',{msg: message.text, user: socket.username, created: new Date()})
  })
});

