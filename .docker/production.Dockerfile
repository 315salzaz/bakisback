FROM node:13.12.0-alpine

WORKDIR /app
RUN npm install -g nodemon
COPY . .
RUN npm install

CMD ["sh", "-c", "npm install && npm start"]