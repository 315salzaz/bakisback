//Projektas: Linker4you Back-End
//Vardas Pav: Vilius Kumparskas Povilas Grusas
//Paskirtis: Connection to MongoDB local and server
//Code Line:25
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;


//mongoose.connect('mongodb://' + process.env.MONGO_INITDB_ROOT_USERNAME + ":" + process.env.MONGO_INITDB_ROOT_PASSWORD + '@' + process.env.MONGO_DB_HOST + ':' + process.env.MONGO_DB_PORT + "/" + process.env.MONGO_INITDB_DATABASE + '?authSource=admin')
 mongoose.connect('mongodb://localhost:27017/UrlManager', { useNewUrlParser: true }).then(() => {
    console.log("Connected to MongoDB successfully :)");
//To prevent deprectation warnings (from MongoDB native driver)
}).catch((e) => {
   console.log("Error while attempting to connect to MongoDB");
   console.log(e);
});
//Console.log("Connecting to Mongo", url)

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);


module.exports = {
    mongoose
};