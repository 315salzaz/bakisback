const scheduler = require('node-schedule')
const nodemailer = require('nodemailer')

class DailyChecker {
    static transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: '321salzaz@gmail.com',
            pass: 'linker4you'
        }
    })

    constructor(doDailyCheck) {
        scheduler.scheduleJob({hour: 12}, function() {
            doDailyCheck()
          });
    }

    static sendEmail(userEmail, userLink, userLinkUrl, yesterdayCounter, todayCounter) {
        this.transporter.sendMail({
            from: '321salzaz@gmail.com',
            to: userEmail,
            subject: 'Double link clicks notice!',
            text: 'Your link: ' + userLink + ' (' + userLinkUrl + ')' + ' reached double the ammount of clicks compared to yesterday! \n'
            + 'Clicks yesterday: ' + yesterdayCounter + '\n'
            + 'Clicks today: ' + todayCounter + '\n'
            + 'Sincerely: linker4you'
          },
          function(error, info){
              if (error) {
                console.log(error)
              } else {
                console.log("EMAIL SENT SUCCESSFULLY")
              }
        })
    }
}

module.exports = DailyChecker;